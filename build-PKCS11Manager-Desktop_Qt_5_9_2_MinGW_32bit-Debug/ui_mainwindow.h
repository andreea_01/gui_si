/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionLoad_PKCS11_library;
    QAction *actionAbout;
    QAction *actionUser_login;
    QAction *actionSO_login;
    QAction *actionLogout;
    QAction *actionInitialize_token;
    QAction *actionUser_login_2;
    QAction *actionSO_login_2;
    QAction *actionUser_PIN;
    QAction *actionSO_PIN;
    QAction *actionUnblock_PIN;
    QAction *actionUser_login_3;
    QAction *actionSO_login_3;
    QAction *actionLogout_2;
    QWidget *centralWidget;
    QTabWidget *tab_data_objects;
    QWidget *tab_basic;
    QScrollArea *scrollArea_info;
    QWidget *scrollAreaWidgetContents;
    QLabel *label_basic_pr_name;
    QFrame *line;
    QLabel *label_basic_pr_value;
    QLabel *label_info_libr_info;
    QFrame *line_2;
    QLabel *label_slot_info;
    QFrame *line_5;
    QListWidget *list_library_info;
    QListWidget *list_slot_info;
    QLabel *label_info;
    QWidget *tab_mechanisms;
    QLabel *label_mechanisms_info;
    QTreeWidget *treeWidget;
    QWidget *tab_certificates;
    QListWidget *list_certificates;
    QLabel *label_certificates_info;
    QWidget *tab_keys;
    QLabel *label_keys_info;
    QListWidget *list_keys;
    QListWidget *list_keys_3;
    QLabel *label_keys_info_3;
    QWidget *tab;
    QLabel *label_keys_info_2;
    QListWidget *list_keys_2;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuApplication;
    QMenu *menuSlot;
    QMenu *menuToken;
    QMenu *menuChange_PIN;
    QMenu *menuLogin;
    QMenu *menuObject;
    QMenu *menuTools;
    QMenu *menuHelp;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(671, 436);
        actionLoad_PKCS11_library = new QAction(MainWindow);
        actionLoad_PKCS11_library->setObjectName(QStringLiteral("actionLoad_PKCS11_library"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        actionUser_login = new QAction(MainWindow);
        actionUser_login->setObjectName(QStringLiteral("actionUser_login"));
        actionSO_login = new QAction(MainWindow);
        actionSO_login->setObjectName(QStringLiteral("actionSO_login"));
        actionLogout = new QAction(MainWindow);
        actionLogout->setObjectName(QStringLiteral("actionLogout"));
        actionInitialize_token = new QAction(MainWindow);
        actionInitialize_token->setObjectName(QStringLiteral("actionInitialize_token"));
        actionUser_login_2 = new QAction(MainWindow);
        actionUser_login_2->setObjectName(QStringLiteral("actionUser_login_2"));
        actionSO_login_2 = new QAction(MainWindow);
        actionSO_login_2->setObjectName(QStringLiteral("actionSO_login_2"));
        actionUser_PIN = new QAction(MainWindow);
        actionUser_PIN->setObjectName(QStringLiteral("actionUser_PIN"));
        actionSO_PIN = new QAction(MainWindow);
        actionSO_PIN->setObjectName(QStringLiteral("actionSO_PIN"));
        actionUnblock_PIN = new QAction(MainWindow);
        actionUnblock_PIN->setObjectName(QStringLiteral("actionUnblock_PIN"));
        actionUser_login_3 = new QAction(MainWindow);
        actionUser_login_3->setObjectName(QStringLiteral("actionUser_login_3"));
        actionSO_login_3 = new QAction(MainWindow);
        actionSO_login_3->setObjectName(QStringLiteral("actionSO_login_3"));
        actionLogout_2 = new QAction(MainWindow);
        actionLogout_2->setObjectName(QStringLiteral("actionLogout_2"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tab_data_objects = new QTabWidget(centralWidget);
        tab_data_objects->setObjectName(QStringLiteral("tab_data_objects"));
        tab_data_objects->setGeometry(QRect(10, 0, 661, 351));
        tab_basic = new QWidget();
        tab_basic->setObjectName(QStringLiteral("tab_basic"));
        scrollArea_info = new QScrollArea(tab_basic);
        scrollArea_info->setObjectName(QStringLiteral("scrollArea_info"));
        scrollArea_info->setGeometry(QRect(10, 30, 631, 291));
        scrollArea_info->setAutoFillBackground(false);
        scrollArea_info->setWidgetResizable(false);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 629, 279));
        label_basic_pr_name = new QLabel(scrollAreaWidgetContents);
        label_basic_pr_name->setObjectName(QStringLiteral("label_basic_pr_name"));
        label_basic_pr_name->setGeometry(QRect(10, 0, 101, 16));
        QFont font;
        font.setPointSize(9);
        label_basic_pr_name->setFont(font);
        line = new QFrame(scrollAreaWidgetContents);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(90, 0, 20, 16));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        label_basic_pr_value = new QLabel(scrollAreaWidgetContents);
        label_basic_pr_value->setObjectName(QStringLiteral("label_basic_pr_value"));
        label_basic_pr_value->setGeometry(QRect(110, 0, 101, 16));
        label_basic_pr_value->setFont(font);
        label_info_libr_info = new QLabel(scrollAreaWidgetContents);
        label_info_libr_info->setObjectName(QStringLiteral("label_info_libr_info"));
        label_info_libr_info->setGeometry(QRect(10, 20, 81, 16));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        label_info_libr_info->setFont(font1);
        line_2 = new QFrame(scrollAreaWidgetContents);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(90, 20, 531, 16));
        line_2->setAutoFillBackground(false);
        line_2->setFrameShadow(QFrame::Plain);
        line_2->setLineWidth(1);
        line_2->setFrameShape(QFrame::HLine);
        label_slot_info = new QLabel(scrollAreaWidgetContents);
        label_slot_info->setObjectName(QStringLiteral("label_slot_info"));
        label_slot_info->setGeometry(QRect(10, 150, 61, 16));
        label_slot_info->setFont(font1);
        line_5 = new QFrame(scrollAreaWidgetContents);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(70, 150, 551, 20));
        line_5->setAutoFillBackground(false);
        line_5->setFrameShadow(QFrame::Plain);
        line_5->setLineWidth(1);
        line_5->setFrameShape(QFrame::HLine);
        list_library_info = new QListWidget(scrollAreaWidgetContents);
        list_library_info->setObjectName(QStringLiteral("list_library_info"));
        list_library_info->setGeometry(QRect(10, 40, 611, 101));
        list_slot_info = new QListWidget(scrollAreaWidgetContents);
        list_slot_info->setObjectName(QStringLiteral("list_slot_info"));
        list_slot_info->setGeometry(QRect(10, 170, 611, 111));
        scrollArea_info->setWidget(scrollAreaWidgetContents);
        label_info = new QLabel(tab_basic);
        label_info->setObjectName(QStringLiteral("label_info"));
        label_info->setGeometry(QRect(10, 10, 511, 16));
        tab_data_objects->addTab(tab_basic, QString());
        tab_mechanisms = new QWidget();
        tab_mechanisms->setObjectName(QStringLiteral("tab_mechanisms"));
        label_mechanisms_info = new QLabel(tab_mechanisms);
        label_mechanisms_info->setObjectName(QStringLiteral("label_mechanisms_info"));
        label_mechanisms_info->setGeometry(QRect(10, 10, 411, 20));
        treeWidget = new QTreeWidget(tab_mechanisms);
        new QTreeWidgetItem(treeWidget);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->setGeometry(QRect(5, 40, 641, 271));
        tab_data_objects->addTab(tab_mechanisms, QString());
        tab_certificates = new QWidget();
        tab_certificates->setObjectName(QStringLiteral("tab_certificates"));
        list_certificates = new QListWidget(tab_certificates);
        list_certificates->setObjectName(QStringLiteral("list_certificates"));
        list_certificates->setGeometry(QRect(10, 51, 631, 261));
        label_certificates_info = new QLabel(tab_certificates);
        label_certificates_info->setObjectName(QStringLiteral("label_certificates_info"));
        label_certificates_info->setGeometry(QRect(10, 10, 411, 20));
        tab_data_objects->addTab(tab_certificates, QString());
        tab_keys = new QWidget();
        tab_keys->setObjectName(QStringLiteral("tab_keys"));
        label_keys_info = new QLabel(tab_keys);
        label_keys_info->setObjectName(QStringLiteral("label_keys_info"));
        label_keys_info->setGeometry(QRect(10, 9, 411, 20));
        list_keys = new QListWidget(tab_keys);
        list_keys->setObjectName(QStringLiteral("list_keys"));
        list_keys->setGeometry(QRect(10, 50, 631, 261));
        list_keys_3 = new QListWidget(tab_keys);
        list_keys_3->setObjectName(QStringLiteral("list_keys_3"));
        list_keys_3->setGeometry(QRect(10, 50, 631, 261));
        label_keys_info_3 = new QLabel(tab_keys);
        label_keys_info_3->setObjectName(QStringLiteral("label_keys_info_3"));
        label_keys_info_3->setGeometry(QRect(10, 9, 411, 20));
        tab_data_objects->addTab(tab_keys, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        label_keys_info_2 = new QLabel(tab);
        label_keys_info_2->setObjectName(QStringLiteral("label_keys_info_2"));
        label_keys_info_2->setGeometry(QRect(10, 10, 411, 20));
        list_keys_2 = new QListWidget(tab);
        list_keys_2->setObjectName(QStringLiteral("list_keys_2"));
        list_keys_2->setGeometry(QRect(10, 51, 631, 261));
        tab_data_objects->addTab(tab, QString());
        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 671, 21));
        menuApplication = new QMenu(menuBar);
        menuApplication->setObjectName(QStringLiteral("menuApplication"));
        menuSlot = new QMenu(menuBar);
        menuSlot->setObjectName(QStringLiteral("menuSlot"));
        menuToken = new QMenu(menuBar);
        menuToken->setObjectName(QStringLiteral("menuToken"));
        menuChange_PIN = new QMenu(menuToken);
        menuChange_PIN->setObjectName(QStringLiteral("menuChange_PIN"));
        menuLogin = new QMenu(menuToken);
        menuLogin->setObjectName(QStringLiteral("menuLogin"));
        menuObject = new QMenu(menuBar);
        menuObject->setObjectName(QStringLiteral("menuObject"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QStringLiteral("menuTools"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuApplication->menuAction());
        menuBar->addAction(menuSlot->menuAction());
        menuBar->addAction(menuToken->menuAction());
        menuBar->addAction(menuObject->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuApplication->addAction(actionLoad_PKCS11_library);
        menuToken->addAction(menuLogin->menuAction());
        menuToken->addAction(actionLogout_2);
        menuToken->addAction(menuChange_PIN->menuAction());
        menuToken->addSeparator();
        menuToken->addAction(actionInitialize_token);
        menuToken->addAction(actionUnblock_PIN);
        menuChange_PIN->addAction(actionUser_PIN);
        menuChange_PIN->addAction(actionSO_PIN);
        menuLogin->addAction(actionUser_login_3);
        menuLogin->addAction(actionSO_login_3);
        menuHelp->addAction(actionAbout);

        retranslateUi(MainWindow);

        tab_data_objects->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionLoad_PKCS11_library->setText(QApplication::translate("MainWindow", "Load PKCS11 library", Q_NULLPTR));
        actionAbout->setText(QApplication::translate("MainWindow", "About", Q_NULLPTR));
        actionUser_login->setText(QApplication::translate("MainWindow", "User login", Q_NULLPTR));
        actionSO_login->setText(QApplication::translate("MainWindow", "SO login", Q_NULLPTR));
        actionLogout->setText(QApplication::translate("MainWindow", "Logout", Q_NULLPTR));
        actionInitialize_token->setText(QApplication::translate("MainWindow", "Initialize token", Q_NULLPTR));
        actionUser_login_2->setText(QApplication::translate("MainWindow", "User login", Q_NULLPTR));
        actionSO_login_2->setText(QApplication::translate("MainWindow", "SO login", Q_NULLPTR));
        actionUser_PIN->setText(QApplication::translate("MainWindow", "User PIN", Q_NULLPTR));
        actionSO_PIN->setText(QApplication::translate("MainWindow", "SO PIN", Q_NULLPTR));
        actionUnblock_PIN->setText(QApplication::translate("MainWindow", "Unblock PIN", Q_NULLPTR));
        actionUser_login_3->setText(QApplication::translate("MainWindow", "User  login", Q_NULLPTR));
        actionSO_login_3->setText(QApplication::translate("MainWindow", "SO login", Q_NULLPTR));
        actionLogout_2->setText(QApplication::translate("MainWindow", "Logout", Q_NULLPTR));
        label_basic_pr_name->setText(QApplication::translate("MainWindow", "Property name", Q_NULLPTR));
        label_basic_pr_value->setText(QApplication::translate("MainWindow", "Property value", Q_NULLPTR));
        label_info_libr_info->setText(QApplication::translate("MainWindow", "Library info", Q_NULLPTR));
        label_slot_info->setText(QApplication::translate("MainWindow", "Slot info", Q_NULLPTR));
        label_info->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        tab_data_objects->setTabText(tab_data_objects->indexOf(tab_basic), QApplication::translate("MainWindow", "Basic info", Q_NULLPTR));
        label_mechanisms_info->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(4, QApplication::translate("MainWindow", "...", Q_NULLPTR));
        ___qtreewidgetitem->setText(3, QApplication::translate("MainWindow", "Flags", Q_NULLPTR));
        ___qtreewidgetitem->setText(2, QApplication::translate("MainWindow", "Max key size", Q_NULLPTR));
        ___qtreewidgetitem->setText(1, QApplication::translate("MainWindow", "Min key size", Q_NULLPTR));
        ___qtreewidgetitem->setText(0, QApplication::translate("MainWindow", "Mechanism", Q_NULLPTR));

        const bool __sortingEnabled = treeWidget->isSortingEnabled();
        treeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = treeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(3, QApplication::translate("MainWindow", "bla", Q_NULLPTR));
        ___qtreewidgetitem1->setText(2, QApplication::translate("MainWindow", "bla", Q_NULLPTR));
        ___qtreewidgetitem1->setText(1, QApplication::translate("MainWindow", "bla", Q_NULLPTR));
        ___qtreewidgetitem1->setText(0, QApplication::translate("MainWindow", "CKM...", Q_NULLPTR));
        treeWidget->setSortingEnabled(__sortingEnabled);

        tab_data_objects->setTabText(tab_data_objects->indexOf(tab_mechanisms), QApplication::translate("MainWindow", "Mechanisms", Q_NULLPTR));
        label_certificates_info->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        tab_data_objects->setTabText(tab_data_objects->indexOf(tab_certificates), QApplication::translate("MainWindow", "Certificates", Q_NULLPTR));
        label_keys_info->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        label_keys_info_3->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        tab_data_objects->setTabText(tab_data_objects->indexOf(tab_keys), QApplication::translate("MainWindow", "Keys", Q_NULLPTR));
        label_keys_info_2->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        tab_data_objects->setTabText(tab_data_objects->indexOf(tab), QApplication::translate("MainWindow", "Data objects", Q_NULLPTR));
        menuApplication->setTitle(QApplication::translate("MainWindow", "Application", Q_NULLPTR));
        menuSlot->setTitle(QApplication::translate("MainWindow", "Slot", Q_NULLPTR));
        menuToken->setTitle(QApplication::translate("MainWindow", "Token", Q_NULLPTR));
        menuChange_PIN->setTitle(QApplication::translate("MainWindow", "Change PIN", Q_NULLPTR));
        menuLogin->setTitle(QApplication::translate("MainWindow", "Login", Q_NULLPTR));
        menuObject->setTitle(QApplication::translate("MainWindow", "Object", Q_NULLPTR));
        menuTools->setTitle(QApplication::translate("MainWindow", "Tools", Q_NULLPTR));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
