#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->label_info->setText("Basic information about PKCS#11 library, slot, token");
}

MainWindow::~MainWindow()
{
    delete ui;
}
